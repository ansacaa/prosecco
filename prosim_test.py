from pm4pybpmn.objects.bpmn.importer.bpmn20 import import_bpmn
from prosecco.properties import initializeSimulationProperties
from prosecco.simulator import Simulator
from datetime import datetime

import json

path = "./diagrams/diagram_credit_2.bpmn"
graph = import_bpmn(path)
props = initializeSimulationProperties(json.load(open('./diagrams/global.json')), json.load(open('./diagrams/simulation.json')))

simulator = Simulator(graph, props)
simulator.run(datetime(2020, 9, 9), endDate = datetime(2020, 9, 16))
print("Done")